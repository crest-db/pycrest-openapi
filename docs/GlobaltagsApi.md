# hep.crest.client.GlobaltagsApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_global_tag**](GlobaltagsApi.md#create_global_tag) | **POST** /globaltags | Create a GlobalTag in the database.
[**find_global_tag**](GlobaltagsApi.md#find_global_tag) | **GET** /globaltags/{name} | Finds a GlobalTagDto by name
[**find_global_tag_fetch_tags**](GlobaltagsApi.md#find_global_tag_fetch_tags) | **GET** /globaltags/{name}/tags | Finds a TagDtos lists associated to the global tag name in input.
[**list_global_tags**](GlobaltagsApi.md#list_global_tags) | **GET** /globaltags | Finds a GlobalTagDtos lists.


# **create_global_tag**
> GlobalTagDto create_global_tag()

Create a GlobalTag in the database.

This method allows to insert a GlobalTag.Arguments: GlobalTagDto should be provided in the body as a JSON file.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltags_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltags_api.GlobaltagsApi(api_client)
    force = "false" # str | force: tell the server if it should use or not the insertion time provided {default: false} (optional) if omitted the server will use the default value of "false"
    global_tag_dto = GlobalTagDto(
        name="name_example",
        validity=1,
        description="description_example",
        release="release_example",
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        snapshot_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        scenario="scenario_example",
        workflow="workflow_example",
        type="type_example",
        snapshot_time_milli=1,
        insertion_time_milli=1,
    ) # GlobalTagDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a GlobalTag in the database.
        api_response = api_instance.create_global_tag(force=force, global_tag_dto=global_tag_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagsApi->create_global_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **force** | **str**| force: tell the server if it should use or not the insertion time provided {default: false} | [optional] if omitted the server will use the default value of "false"
 **global_tag_dto** | [**GlobalTagDto**](GlobalTagDto.md)|  | [optional]

### Return type

[**GlobalTagDto**](GlobalTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_global_tag**
> GlobalTagSetDto find_global_tag(name)

Finds a GlobalTagDto by name

This method will search for a global tag with the given name. Only one global tag should be returned.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltags_api
from hep.crest.client.model.global_tag_set_dto import GlobalTagSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltags_api.GlobaltagsApi(api_client)
    name = "name_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Finds a GlobalTagDto by name
        api_response = api_instance.find_global_tag(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagsApi->find_global_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |

### Return type

[**GlobalTagSetDto**](GlobalTagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_global_tag_fetch_tags**
> TagSetDto find_global_tag_fetch_tags(name)

Finds a TagDtos lists associated to the global tag name in input.

This method allows to trace a global tag.Arguments: record=<record> filter output by record, label=<label> filter output by label

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltags_api
from hep.crest.client.model.tag_set_dto import TagSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltags_api.GlobaltagsApi(api_client)
    name = "name_example" # str | 
    record = "none" # str | record:  the record string {} (optional) if omitted the server will use the default value of "none"
    label = "none" # str | label:  the label string {} (optional) if omitted the server will use the default value of "none"

    # example passing only required values which don't have defaults set
    try:
        # Finds a TagDtos lists associated to the global tag name in input.
        api_response = api_instance.find_global_tag_fetch_tags(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagsApi->find_global_tag_fetch_tags: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a TagDtos lists associated to the global tag name in input.
        api_response = api_instance.find_global_tag_fetch_tags(name, record=record, label=label)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagsApi->find_global_tag_fetch_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |
 **record** | **str**| record:  the record string {} | [optional] if omitted the server will use the default value of "none"
 **label** | **str**| label:  the label string {} | [optional] if omitted the server will use the default value of "none"

### Return type

[**TagSetDto**](TagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_global_tags**
> GlobalTagSetDto list_global_tags()

Finds a GlobalTagDtos lists.

This method allows to perform search and sorting. Arguments: name=<pattern>, workflow, scenario, release, validity, description page={ipage}, size={isize}, sort=<sortpattern>. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltags_api
from hep.crest.client.model.global_tag_set_dto import GlobalTagSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltags_api.GlobaltagsApi(api_client)
    name = "all" # str | the global tag name search pattern {none} (optional) if omitted the server will use the default value of "all"
    workflow = "workflow_example" # str | the global tag workflow search pattern {none} (optional)
    scenario = "scenario_example" # str | the global tag scenario search pattern {none} (optional)
    release = "release_example" # str | the global tag release search pattern {none} (optional)
    validity = 1 # int | the global tag validity low limit {x>=validity} (optional)
    description = "description_example" # str | the global tag description search pattern {none} (optional)
    page = 0 # int | page: the page number {0} (optional) if omitted the server will use the default value of 0
    size = 1000 # int | size: the page size {1000} (optional) if omitted the server will use the default value of 1000
    sort = "name:ASC" # str | sort: the sort pattern {name:ASC} (optional) if omitted the server will use the default value of "name:ASC"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a GlobalTagDtos lists.
        api_response = api_instance.list_global_tags(name=name, workflow=workflow, scenario=scenario, release=release, validity=validity, description=description, page=page, size=size, sort=sort)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagsApi->list_global_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| the global tag name search pattern {none} | [optional] if omitted the server will use the default value of "all"
 **workflow** | **str**| the global tag workflow search pattern {none} | [optional]
 **scenario** | **str**| the global tag scenario search pattern {none} | [optional]
 **release** | **str**| the global tag release search pattern {none} | [optional]
 **validity** | **int**| the global tag validity low limit {x&gt;&#x3D;validity} | [optional]
 **description** | **str**| the global tag description search pattern {none} | [optional]
 **page** | **int**| page: the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| size: the page size {1000} | [optional] if omitted the server will use the default value of 1000
 **sort** | **str**| sort: the sort pattern {name:ASC} | [optional] if omitted the server will use the default value of "name:ASC"

### Return type

[**GlobalTagSetDto**](GlobalTagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

