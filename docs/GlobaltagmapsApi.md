# hep.crest.client.GlobaltagmapsApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_global_tag_map**](GlobaltagmapsApi.md#create_global_tag_map) | **POST** /globaltagmaps | Create a GlobalTagMap in the database.
[**delete_global_tag_map**](GlobaltagmapsApi.md#delete_global_tag_map) | **DELETE** /globaltagmaps/{name} | Delete GlobalTagMapDto lists.
[**find_global_tag_map**](GlobaltagmapsApi.md#find_global_tag_map) | **GET** /globaltagmaps/{name} | Find GlobalTagMapDto lists.


# **create_global_tag_map**
> GlobalTagMapDto create_global_tag_map()

Create a GlobalTagMap in the database.

This method allows to insert a GlobalTagMap.Arguments: GlobalTagMapDto should be provided in the body as a JSON file.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltagmaps_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.global_tag_map_dto import GlobalTagMapDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltagmaps_api.GlobaltagmapsApi(api_client)
    global_tag_map_dto = GlobalTagMapDto(
        global_tag_name="global_tag_name_example",
        record="record_example",
        label="label_example",
        tag_name="tag_name_example",
    ) # GlobalTagMapDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a GlobalTagMap in the database.
        api_response = api_instance.create_global_tag_map(global_tag_map_dto=global_tag_map_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagmapsApi->create_global_tag_map: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **global_tag_map_dto** | [**GlobalTagMapDto**](GlobalTagMapDto.md)|  | [optional]

### Return type

[**GlobalTagMapDto**](GlobalTagMapDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_global_tag_map**
> GlobalTagMapSetDto delete_global_tag_map(name, )

Delete GlobalTagMapDto lists.

This method search for mappings using the global tag name and deletes all mappings.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltagmaps_api
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltagmaps_api.GlobaltagmapsApi(api_client)
    name = "name_example" # str | the global tag name
    record = "record_example" # str | record: the record. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Delete GlobalTagMapDto lists.
        api_response = api_instance.delete_global_tag_map(name, )
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagmapsApi->delete_global_tag_map: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Delete GlobalTagMapDto lists.
        api_response = api_instance.delete_global_tag_map(name, record=record)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagmapsApi->delete_global_tag_map: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| the global tag name |
 **label** | **str**| label: the generic name labelling all tags of a certain kind. | defaults to "none"
 **tagname** | **str**| tagname: the name of the tag associated. | defaults to "none"
 **record** | **str**| record: the record. | [optional]

### Return type

[**GlobalTagMapSetDto**](GlobalTagMapSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_global_tag_map**
> GlobalTagMapSetDto find_global_tag_map(name)

Find GlobalTagMapDto lists.

This method search for mappings using the global tag name.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import globaltagmaps_api
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = globaltagmaps_api.GlobaltagmapsApi(api_client)
    name = "name_example" # str | 
    x_crest_map_mode = "Trace" # str | If the mode is BackTrace then it will search for global tags containing the tag <name> (optional) if omitted the server will use the default value of "Trace"

    # example passing only required values which don't have defaults set
    try:
        # Find GlobalTagMapDto lists.
        api_response = api_instance.find_global_tag_map(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagmapsApi->find_global_tag_map: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Find GlobalTagMapDto lists.
        api_response = api_instance.find_global_tag_map(name, x_crest_map_mode=x_crest_map_mode)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling GlobaltagmapsApi->find_global_tag_map: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |
 **x_crest_map_mode** | **str**| If the mode is BackTrace then it will search for global tags containing the tag &lt;name&gt; | [optional] if omitted the server will use the default value of "Trace"

### Return type

[**GlobalTagMapSetDto**](GlobalTagMapSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

