# HTTPResponse

general response object that can be used for POST and PUT methods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | HTTP status code of the response | 
**message** | **str** |  | 
**timestamp** | **datetime** |  | [optional] 
**error** | **str** |  | [optional] 
**type** | **str** | A generic string specifying the exception type. | [optional] 
**id** | **str** | path or URI of the requested or generated resource | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


