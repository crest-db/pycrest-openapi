# hep.crest.client.AdminApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**remove_global_tag**](AdminApi.md#remove_global_tag) | **DELETE** /admin/globaltags/{name} | Remove a GlobalTag from the database.
[**remove_tag**](AdminApi.md#remove_tag) | **DELETE** /admin/tags/{name} | Remove a Tag from the database.
[**update_global_tag**](AdminApi.md#update_global_tag) | **PUT** /admin/globaltags/{name} | Update a GlobalTag in the database.


# **remove_global_tag**
> remove_global_tag(name)

Remove a GlobalTag from the database.

This method allows to remove a GlobalTag.Arguments: the name has to uniquely identify a global tag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import admin_api
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = admin_api.AdminApi(api_client)
    name = "name_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Remove a GlobalTag from the database.
        api_instance.remove_global_tag(name)
    except hep.crest.client.ApiException as e:
        print("Exception when calling AdminApi->remove_global_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_tag**
> remove_tag(name)

Remove a Tag from the database.

This method allows to remove a Tag.Arguments: the name has to uniquely identify a tag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import admin_api
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = admin_api.AdminApi(api_client)
    name = "name_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Remove a Tag from the database.
        api_instance.remove_tag(name)
    except hep.crest.client.ApiException as e:
        print("Exception when calling AdminApi->remove_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_global_tag**
> GlobalTagDto update_global_tag(name)

Update a GlobalTag in the database.

This method allows to update a GlobalTag.Arguments: the name has to uniquely identify a global tag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import admin_api
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = admin_api.AdminApi(api_client)
    name = "name_example" # str | 
    global_tag_dto = GlobalTagDto(
        name="name_example",
        validity=1,
        description="description_example",
        release="release_example",
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        snapshot_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        scenario="scenario_example",
        workflow="workflow_example",
        type="type_example",
        snapshot_time_milli=1,
        insertion_time_milli=1,
    ) # GlobalTagDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a GlobalTag in the database.
        api_response = api_instance.update_global_tag(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling AdminApi->update_global_tag: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a GlobalTag in the database.
        api_response = api_instance.update_global_tag(name, global_tag_dto=global_tag_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling AdminApi->update_global_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  |
 **global_tag_dto** | [**GlobalTagDto**](GlobalTagDto.md)|  | [optional]

### Return type

[**GlobalTagDto**](GlobalTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

