# hep.crest.client.MonitoringApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_payload_tag_info**](MonitoringApi.md#list_payload_tag_info) | **GET** /monitoring/payloads | Retrieves monitoring information on payload as a list of PayloadTagInfoDtos.


# **list_payload_tag_info**
> PayloadTagInfoSetDto list_payload_tag_info()

Retrieves monitoring information on payload as a list of PayloadTagInfoDtos.

This method allows to perform search and sorting.Arguments: tagname=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import monitoring_api
from hep.crest.client.model.payload_tag_info_set_dto import PayloadTagInfoSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = monitoring_api.MonitoringApi(api_client)
    tagname = "none" # str | tagname: the search pattern {none} (optional) if omitted the server will use the default value of "none"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves monitoring information on payload as a list of PayloadTagInfoDtos.
        api_response = api_instance.list_payload_tag_info(tagname=tagname)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling MonitoringApi->list_payload_tag_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| tagname: the search pattern {none} | [optional] if omitted the server will use the default value of "none"

### Return type

[**PayloadTagInfoSetDto**](PayloadTagInfoSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

