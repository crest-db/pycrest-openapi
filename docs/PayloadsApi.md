# hep.crest.client.PayloadsApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_payload**](PayloadsApi.md#get_payload) | **GET** /payloads/data | Finds a payload resource associated to the hash.
[**list_payloads**](PayloadsApi.md#list_payloads) | **GET** /payloads | Finds Payloads metadata.
[**store_payload_batch**](PayloadsApi.md#store_payload_batch) | **POST** /payloads | Create Payloads in the database, associated to a given iov since list and tag name.
[**update_payload**](PayloadsApi.md#update_payload) | **PUT** /payloads/data | Update a streamerInfo in a payload
[**upload_json**](PayloadsApi.md#upload_json) | **PUT** /payloads | Upload and process large JSON data.


# **get_payload**
> str get_payload(hash, )

Finds a payload resource associated to the hash.

This method retrieves a payload resource. Arguments: hash=<hash> the hash of the payload Depending on the header, this method will either retrieve the data, the metadata of the payload  or the streamerInfo alone. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import payloads_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.payload_dto import PayloadDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = payloads_api.PayloadsApi(api_client)
    hash = "hash_example" # str | hash:  the hash of the payload

    # example passing only required values which don't have defaults set
    try:
        # Finds a payload resource associated to the hash.
        api_response = api_instance.get_payload(hash, )
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->get_payload: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hash** | **str**| hash:  the hash of the payload |
 **format** | **str**| The format of the output data.  It can be : BLOB (default), META (meta data) or STREAMER (streamerInfo).  | defaults to "BLOB"

### Return type

**str**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_payloads**
> PayloadSetDto list_payloads()

Finds Payloads metadata.

This method allows to perform search and sorting. Arguments: hash=<the payload hash>, minsize=<min size>, objectType=<the type> page={ipage}, size={isize}, sort=<sortpattern>. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import payloads_api
from hep.crest.client.model.payload_set_dto import PayloadSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = payloads_api.PayloadsApi(api_client)
    hash = "hash_example" # str | the hash to search {none} (optional)
    object_type = "objectType_example" # str | the objectType to search (optional)
    minsize = 1 # int | the minimum size to search (optional)
    page = 0 # int | page: the page number {0} (optional) if omitted the server will use the default value of 0
    size = 1000 # int | size: the page size {1000} (optional) if omitted the server will use the default value of 1000
    sort = "insertionTime:DESC" # str | sort: the sort pattern {insertionTime:DESC} (optional) if omitted the server will use the default value of "insertionTime:DESC"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds Payloads metadata.
        api_response = api_instance.list_payloads(hash=hash, object_type=object_type, minsize=minsize, page=page, size=size, sort=sort)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->list_payloads: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hash** | **str**| the hash to search {none} | [optional]
 **object_type** | **str**| the objectType to search | [optional]
 **minsize** | **int**| the minimum size to search | [optional]
 **page** | **int**| page: the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| size: the page size {1000} | [optional] if omitted the server will use the default value of 1000
 **sort** | **str**| sort: the sort pattern {insertionTime:DESC} | [optional] if omitted the server will use the default value of "insertionTime:DESC"

### Return type

[**PayloadSetDto**](PayloadSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **store_payload_batch**
> StoreSetDto store_payload_batch(tag, storeset)

Create Payloads in the database, associated to a given iov since list and tag name.

This method allows to insert list of Payloads and IOVs. Payload can be contained in the HASH of the IOV (in case it is a small JSON) or as a reference to external file (FILE). In the first case, the files list can be null. Arguments: tag,version,endtime,objectType,compressionType The header parameter X-Crest-PayloadFormat can be FILE or JSON 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import payloads_api
from hep.crest.client.model.store_set_dto import StoreSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = payloads_api.PayloadsApi(api_client)
    tag = "tag_example" # str | The tag name
    storeset = "storeset_example" # str | the string representing a StoreSetDto in json
    x_crest_payload_format = "FILE" # str | The format of the input data. StoreSetDto entries will have either the content inline (JSON) or stored via external files (FILE).  (optional) if omitted the server will use the default value of "FILE"
    files = [
        open('/path/to/file', 'rb'),
    ] # [file_type] | The payload files as an array of streams (optional)
    object_type = "object_type_example" # str | The object type (optional)
    compression_type = "compression_type_example" # str | The compression type (optional)
    version = "version_example" # str | The version (optional)
    endtime = 1 # int | The end time, shall be set at tag level. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Create Payloads in the database, associated to a given iov since list and tag name.
        api_response = api_instance.store_payload_batch(tag, storeset)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->store_payload_batch: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create Payloads in the database, associated to a given iov since list and tag name.
        api_response = api_instance.store_payload_batch(tag, storeset, x_crest_payload_format=x_crest_payload_format, files=files, object_type=object_type, compression_type=compression_type, version=version, endtime=endtime)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->store_payload_batch: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| The tag name |
 **storeset** | **str**| the string representing a StoreSetDto in json |
 **x_crest_payload_format** | **str**| The format of the input data. StoreSetDto entries will have either the content inline (JSON) or stored via external files (FILE).  | [optional] if omitted the server will use the default value of "FILE"
 **files** | **[file_type]**| The payload files as an array of streams | [optional]
 **object_type** | **str**| The object type | [optional]
 **compression_type** | **str**| The compression type | [optional]
 **version** | **str**| The version | [optional]
 **endtime** | **int**| The end time, shall be set at tag level. | [optional]

### Return type

[**StoreSetDto**](StoreSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_payload**
> PayloadDto update_payload(hash)

Update a streamerInfo in a payload

This method will update the streamerInfo. This is provided via a generic map in the request body containing the key 'streamerInfo' 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import payloads_api
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.payload_dto import PayloadDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = payloads_api.PayloadsApi(api_client)
    hash = "hash_example" # str | hash:  the hash of the payload
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a streamerInfo in a payload
        api_response = api_instance.update_payload(hash)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->update_payload: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a streamerInfo in a payload
        api_response = api_instance.update_payload(hash, generic_map=generic_map)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->update_payload: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hash** | **str**| hash:  the hash of the payload |
 **generic_map** | [**GenericMap**](GenericMap.md)|  | [optional]

### Return type

[**PayloadDto**](PayloadDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_json**
> StoreSetDto upload_json(tag, storeset)

Upload and process large JSON data.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import payloads_api
from hep.crest.client.model.store_set_dto import StoreSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = payloads_api.PayloadsApi(api_client)
    tag = "tag_example" # str | The tag name
    storeset = open('/path/to/file', 'rb') # file_type | the string representing a StoreSetDto in json
    object_type = "object_type_example" # str | The object type (optional)
    compression_type = "compression_type_example" # str | The compression type (optional)
    version = "version_example" # str | The version (optional)
    endtime = 1 # int | The end time, shall be set at tag level. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Upload and process large JSON data.
        api_response = api_instance.upload_json(tag, storeset)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->upload_json: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Upload and process large JSON data.
        api_response = api_instance.upload_json(tag, storeset, object_type=object_type, compression_type=compression_type, version=version, endtime=endtime)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling PayloadsApi->upload_json: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| The tag name |
 **storeset** | **file_type**| the string representing a StoreSetDto in json |
 **object_type** | **str**| The object type | [optional]
 **compression_type** | **str**| The compression type | [optional]
 **version** | **str**| The version | [optional]
 **endtime** | **int**| The end time, shall be set at tag level. | [optional]

### Return type

[**StoreSetDto**](StoreSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

