# hep.crest.client.TagsApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_tag**](TagsApi.md#create_tag) | **POST** /tags | Create a Tag in the database.
[**create_tag_meta**](TagsApi.md#create_tag_meta) | **POST** /tags/{name}/meta | Create a TagMeta in the database.
[**find_tag**](TagsApi.md#find_tag) | **GET** /tags/{name} | Finds a TagDto by name
[**find_tag_meta**](TagsApi.md#find_tag_meta) | **GET** /tags/{name}/meta | Finds a TagMetaDto by name
[**list_tags**](TagsApi.md#list_tags) | **GET** /tags | Finds a TagDtos lists.
[**update_tag**](TagsApi.md#update_tag) | **PUT** /tags/{name} | Update a TagDto by name
[**update_tag_meta**](TagsApi.md#update_tag_meta) | **PUT** /tags/{name}/meta | Update a TagMetaDto by name


# **create_tag**
> TagDto create_tag()

Create a Tag in the database.

This method allows to insert a Tag.Arguments: TagDto should be provided in the body as a JSON file.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_dto import TagDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    tag_dto = TagDto(
        name="name_example",
        time_type="time_type_example",
        payload_spec="payload_spec_example",
        synchronization="synchronization_example",
        description="description_example",
        last_validated_time=1,
        end_of_validity=1,
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        modification_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
    ) # TagDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a Tag in the database.
        api_response = api_instance.create_tag(tag_dto=tag_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->create_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag_dto** | [**TagDto**](TagDto.md)|  | [optional]

### Return type

[**TagDto**](TagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_tag_meta**
> TagMetaDto create_tag_meta(name)

Create a TagMeta in the database.

This method allows to insert a TagMeta.Arguments: TagMetaDto should be provided in the body as a JSON file.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_meta_dto import TagMetaDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "name_example" # str | name: the tag name
    tag_meta_dto = TagMetaDto(
        tag_name="tag_name_example",
        description="description_example",
        chansize=1,
        colsize=1,
        tag_info="tag_info_example",
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
    ) # TagMetaDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Create a TagMeta in the database.
        api_response = api_instance.create_tag_meta(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->create_tag_meta: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a TagMeta in the database.
        api_response = api_instance.create_tag_meta(name, tag_meta_dto=tag_meta_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->create_tag_meta: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| name: the tag name |
 **tag_meta_dto** | [**TagMetaDto**](TagMetaDto.md)|  | [optional]

### Return type

[**TagMetaDto**](TagMetaDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_tag**
> TagSetDto find_tag(name)

Finds a TagDto by name

This method will search for a tag with the given name. Only one tag should be returned.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.tag_set_dto import TagSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "name_example" # str | name: the tag name

    # example passing only required values which don't have defaults set
    try:
        # Finds a TagDto by name
        api_response = api_instance.find_tag(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->find_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| name: the tag name |

### Return type

[**TagSetDto**](TagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_tag_meta**
> TagMetaSetDto find_tag_meta(name)

Finds a TagMetaDto by name

This method will search for a tag metadata with the given name. Only one tag should be returned.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_meta_set_dto import TagMetaSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "name_example" # str | name: the tag name

    # example passing only required values which don't have defaults set
    try:
        # Finds a TagMetaDto by name
        api_response = api_instance.find_tag_meta(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->find_tag_meta: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| name: the tag name |

### Return type

[**TagMetaSetDto**](TagMetaSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_tags**
> TagSetDto list_tags()

Finds a TagDtos lists.

This method allows to perform search and sorting. Arguments: name=<pattern>, objectType, timeType, description page={ipage}, size={isize}, sort=<sortpattern>. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.tag_set_dto import TagSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "all" # str | the tag name search pattern {all} (optional) if omitted the server will use the default value of "all"
    time_type = "timeType_example" # str | the tag timeType {none} (optional)
    object_type = "objectType_example" # str | the tag objectType search pattern {none} (optional)
    description = "description_example" # str | the global tag description search pattern {none} (optional)
    page = 0 # int | page: the page number {0} (optional) if omitted the server will use the default value of 0
    size = 1000 # int | size: the page size {1000} (optional) if omitted the server will use the default value of 1000
    sort = "name:ASC" # str | sort: the sort pattern {name:ASC} (optional) if omitted the server will use the default value of "name:ASC"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a TagDtos lists.
        api_response = api_instance.list_tags(name=name, time_type=time_type, object_type=object_type, description=description, page=page, size=size, sort=sort)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->list_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| the tag name search pattern {all} | [optional] if omitted the server will use the default value of "all"
 **time_type** | **str**| the tag timeType {none} | [optional]
 **object_type** | **str**| the tag objectType search pattern {none} | [optional]
 **description** | **str**| the global tag description search pattern {none} | [optional]
 **page** | **int**| page: the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| size: the page size {1000} | [optional] if omitted the server will use the default value of 1000
 **sort** | **str**| sort: the sort pattern {name:ASC} | [optional] if omitted the server will use the default value of "name:ASC"

### Return type

[**TagSetDto**](TagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_tag**
> TagDto update_tag(name)

Update a TagDto by name

This method will search for a tag with the given name, and update its content for the provided body fields. Only the following fields can be updated: description, timeType, objectTime, endOfValidity, lastValidatedTime.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_dto import TagDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "name_example" # str | name: the tag name
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a TagDto by name
        api_response = api_instance.update_tag(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->update_tag: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a TagDto by name
        api_response = api_instance.update_tag(name, generic_map=generic_map)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->update_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| name: the tag name |
 **generic_map** | [**GenericMap**](GenericMap.md)|  | [optional]

### Return type

[**TagDto**](TagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_tag_meta**
> TagMetaDto update_tag_meta(name)

Update a TagMetaDto by name

This method will search for a tag with the given name, and update its content for the provided body fields. Only the following fields can be updated: description, timeType, objectTime, endOfValidity, lastValidatedTime.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import tags_api
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_meta_dto import TagMetaDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tags_api.TagsApi(api_client)
    name = "name_example" # str | name: the tag name
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a TagMetaDto by name
        api_response = api_instance.update_tag_meta(name)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->update_tag_meta: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a TagMetaDto by name
        api_response = api_instance.update_tag_meta(name, generic_map=generic_map)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling TagsApi->update_tag_meta: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| name: the tag name |
 **generic_map** | [**GenericMap**](GenericMap.md)|  | [optional]

### Return type

[**TagMetaDto**](TagMetaDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

