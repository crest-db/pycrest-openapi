# hep.crest.client.IovsApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**find_all_iovs**](IovsApi.md#find_all_iovs) | **GET** /iovs | Finds a IovDtos lists.
[**get_size_by_tag**](IovsApi.md#get_size_by_tag) | **GET** /iovs/size | Get the number o iovs for tags matching pattern.
[**select_iov_payloads**](IovsApi.md#select_iov_payloads) | **GET** /iovs/infos | Select iovs and payload meta info for a given tagname and in a given range.
[**store_iov_batch**](IovsApi.md#store_iov_batch) | **POST** /iovs | Create IOVs in the database, associated to a tag name.
[**store_iov_one**](IovsApi.md#store_iov_one) | **PUT** /iovs | Create a single IOV in the database, associated to a tag name.


# **find_all_iovs**
> IovSetDto find_all_iovs()

Finds a IovDtos lists.

Retrieves IOVs, with parameterizable method and arguments 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import iovs_api
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iovs_api.IovsApi(api_client)
    tagname = "none" # str | the tag name (optional) if omitted the server will use the default value of "none"
    snapshot = 0 # int | snapshot: the snapshot time {0} (optional) if omitted the server will use the default value of 0
    since = "0" # str | the since time as a string {0} (optional) if omitted the server will use the default value of "0"
    until = "INF" # str | the until time as a string {INF} (optional) if omitted the server will use the default value of "INF"
    timeformat = "NUMBER" # str | the format for since and until {number | ms | iso | run-lumi | custom (yyyyMMdd'T'HHmmssX)} If timeformat is equal number, we just parse the argument as a long.  (optional) if omitted the server will use the default value of "NUMBER"
    groupsize = 1 # int | The group size represent the pagination type provided for GROUPS query method.  (optional)
    hash = "hash_example" # str | the hash for searching specific IOV list for a given hash.  (optional)
    page = 0 # int | the page number {0} (optional) if omitted the server will use the default value of 0
    size = 10000 # int | the page size {10000} (optional) if omitted the server will use the default value of 10000
    sort = "id.since:ASC" # str | the sort pattern {id.since:ASC} (optional) if omitted the server will use the default value of "id.since:ASC"
    x_crest_query = "IOVS" # str | The query type. The header parameter X-Crest-Query can be : iovs, ranges, at. The iovs represents an exclusive interval, while ranges and at include previous since. This has an impact on how the since and until ranges are applied.  (optional) if omitted the server will use the default value of "IOVS"
    x_crest_since = "NUMBER" # str | The since type required in the query. It can be : ms, cool. Since and until will be transformed in these units. It differs from timeformat which indicates how to interpret the since and until strings in input.  (optional) if omitted the server will use the default value of "NUMBER"

    # example passing only required values which don't have defaults set
    try:
        # Finds a IovDtos lists.
        api_response = api_instance.find_all_iovs()
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->find_all_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a IovDtos lists.
        api_response = api_instance.find_all_iovs(tagname=tagname, snapshot=snapshot, since=since, until=until, timeformat=timeformat, groupsize=groupsize, hash=hash, page=page, size=size, sort=sort, x_crest_query=x_crest_query, x_crest_since=x_crest_since)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->find_all_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **method** | **str**| the method used will determine which query is executed IOVS, RANGE and AT is a standard IOV query requiring a precise tag name GROUPS is a group query type  | defaults to "IOVS"
 **tagname** | **str**| the tag name | [optional] if omitted the server will use the default value of "none"
 **snapshot** | **int**| snapshot: the snapshot time {0} | [optional] if omitted the server will use the default value of 0
 **since** | **str**| the since time as a string {0} | [optional] if omitted the server will use the default value of "0"
 **until** | **str**| the until time as a string {INF} | [optional] if omitted the server will use the default value of "INF"
 **timeformat** | **str**| the format for since and until {number | ms | iso | run-lumi | custom (yyyyMMdd&#39;T&#39;HHmmssX)} If timeformat is equal number, we just parse the argument as a long.  | [optional] if omitted the server will use the default value of "NUMBER"
 **groupsize** | **int**| The group size represent the pagination type provided for GROUPS query method.  | [optional]
 **hash** | **str**| the hash for searching specific IOV list for a given hash.  | [optional]
 **page** | **int**| the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| the page size {10000} | [optional] if omitted the server will use the default value of 10000
 **sort** | **str**| the sort pattern {id.since:ASC} | [optional] if omitted the server will use the default value of "id.since:ASC"
 **x_crest_query** | **str**| The query type. The header parameter X-Crest-Query can be : iovs, ranges, at. The iovs represents an exclusive interval, while ranges and at include previous since. This has an impact on how the since and until ranges are applied.  | [optional] if omitted the server will use the default value of "IOVS"
 **x_crest_since** | **str**| The since type required in the query. It can be : ms, cool. Since and until will be transformed in these units. It differs from timeformat which indicates how to interpret the since and until strings in input.  | [optional] if omitted the server will use the default value of "NUMBER"

### Return type

[**IovSetDto**](IovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_size_by_tag**
> TagSummarySetDto get_size_by_tag()

Get the number o iovs for tags matching pattern.

This method allows to retrieve the number of iovs in a tag (or pattern). 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import iovs_api
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.tag_summary_set_dto import TagSummarySetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iovs_api.IovsApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the number o iovs for tags matching pattern.
        api_response = api_instance.get_size_by_tag()
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->get_size_by_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the tag name, can be a pattern like MDT% | defaults to "none"

### Return type

[**TagSummarySetDto**](TagSummarySetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **select_iov_payloads**
> IovPayloadSetDto select_iov_payloads()

Select iovs and payload meta info for a given tagname and in a given range.

Retrieve a list of iovs with payload metadata associated. The arguments are: tagname={a tag name}, since={since time as string}, until={until time as string}, snapshot={snapshot time as long}' and timeformat={format of since/until}. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import iovs_api
from hep.crest.client.model.iov_payload_set_dto import IovPayloadSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iovs_api.IovsApi(api_client)
    since = "0" # str | the since time as a string {0} (optional) if omitted the server will use the default value of "0"
    until = "INF" # str | the until time as a string {INF} (optional) if omitted the server will use the default value of "INF"
    timeformat = "number" # str | the format for since and until {number | ms | iso | custom (yyyyMMdd'T'HHmmssX)} If timeformat is equal number, we just parse the argument as a long.  (optional) if omitted the server will use the default value of "number"
    page = 0 # int | the page number {0} (optional) if omitted the server will use the default value of 0
    size = 10000 # int | the page size {10000} (optional) if omitted the server will use the default value of 10000
    sort = "id.since:ASC" # str | the sort pattern {id.since:ASC} (optional) if omitted the server will use the default value of "id.since:ASC"

    # example passing only required values which don't have defaults set
    try:
        # Select iovs and payload meta info for a given tagname and in a given range.
        api_response = api_instance.select_iov_payloads()
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->select_iov_payloads: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Select iovs and payload meta info for a given tagname and in a given range.
        api_response = api_instance.select_iov_payloads(since=since, until=until, timeformat=timeformat, page=page, size=size, sort=sort)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->select_iov_payloads: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the tag name | defaults to "none"
 **since** | **str**| the since time as a string {0} | [optional] if omitted the server will use the default value of "0"
 **until** | **str**| the until time as a string {INF} | [optional] if omitted the server will use the default value of "INF"
 **timeformat** | **str**| the format for since and until {number | ms | iso | custom (yyyyMMdd&#39;T&#39;HHmmssX)} If timeformat is equal number, we just parse the argument as a long.  | [optional] if omitted the server will use the default value of "number"
 **page** | **int**| the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| the page size {10000} | [optional] if omitted the server will use the default value of 10000
 **sort** | **str**| the sort pattern {id.since:ASC} | [optional] if omitted the server will use the default value of "id.since:ASC"

### Return type

[**IovPayloadSetDto**](IovPayloadSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **store_iov_batch**
> IovSetDto store_iov_batch()

Create IOVs in the database, associated to a tag name.

Insert a list of Iovs using an IovSetDto in the request body. It is mandatory to provide an existing tag in input. The referenced payloads should already exists in the DB. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import iovs_api
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iovs_api.IovsApi(api_client)
    iov_set_dto = IovSetDto(None) # IovSetDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create IOVs in the database, associated to a tag name.
        api_response = api_instance.store_iov_batch(iov_set_dto=iov_set_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->store_iov_batch: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iov_set_dto** | [**IovSetDto**](IovSetDto.md)|  | [optional]

### Return type

[**IovSetDto**](IovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **store_iov_one**
> IovSetDto store_iov_one()

Create a single IOV in the database, associated to a tag name.

Insert an Iov using an IovDto in the request body. It is mandatory to provide an existing tag in input. The referenced payloads should already exists in the DB. 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import iovs_api
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.http_response import HTTPResponse
from hep.crest.client.model.iov_dto import IovDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iovs_api.IovsApi(api_client)
    iov_dto = IovDto(
        tag_name="tag_name_example",
        since=1,
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        payload_hash="payload_hash_example",
    ) # IovDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a single IOV in the database, associated to a tag name.
        api_response = api_instance.store_iov_one(iov_dto=iov_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling IovsApi->store_iov_one: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iov_dto** | [**IovDto**](IovDto.md)|  | [optional]

### Return type

[**IovSetDto**](IovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**400** | Bad request |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

