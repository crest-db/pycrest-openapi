# GlobalTagDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**validity** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**release** | **str** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**snapshot_time** | **datetime** |  | [optional] 
**scenario** | **str** |  | [optional] 
**workflow** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**snapshot_time_milli** | **int** |  | [optional] 
**insertion_time_milli** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


