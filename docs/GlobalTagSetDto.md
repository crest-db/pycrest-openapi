# GlobalTagSetDto

An GlobalTagSet containing GlobalTagDto objects.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **int** |  | 
**format** | **str** |  | 
**resources** | [**[GlobalTagDto]**](GlobalTagDto.md) |  | [optional] 
**datatype** | **str** |  | [optional] 
**page** | [**RespPage**](RespPage.md) |  | [optional] 
**filter** | [**GenericMap**](GenericMap.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


