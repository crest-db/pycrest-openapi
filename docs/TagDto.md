# TagDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**time_type** | **str** |  | [optional] 
**payload_spec** | **str** |  | [optional] 
**synchronization** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**last_validated_time** | **int** |  | [optional] 
**end_of_validity** | **int** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**modification_time** | **datetime** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


