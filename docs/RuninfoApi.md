# hep.crest.client.RuninfoApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_run_info**](RuninfoApi.md#create_run_info) | **POST** /runinfo | Create an entry for run information.
[**list_run_info**](RuninfoApi.md#list_run_info) | **GET** /runinfo | Finds a RunLumiInfoDto lists using parameters.
[**update_run_info**](RuninfoApi.md#update_run_info) | **PUT** /runinfo | Update an entry for run information.


# **create_run_info**
> RunLumiSetDto create_run_info()

Create an entry for run information.

Run informations go into a separate table.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import runinfo_api
from hep.crest.client.model.run_lumi_set_dto import RunLumiSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = runinfo_api.RuninfoApi(api_client)
    run_lumi_set_dto = RunLumiSetDto(None) # RunLumiSetDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create an entry for run information.
        api_response = api_instance.create_run_info(run_lumi_set_dto=run_lumi_set_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling RuninfoApi->create_run_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **run_lumi_set_dto** | [**RunLumiSetDto**](RunLumiSetDto.md)|  | [optional]

### Return type

[**RunLumiSetDto**](RunLumiSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_run_info**
> RunLumiSetDto list_run_info()

Finds a RunLumiInfoDto lists using parameters.

This method allows to perform search.Arguments: from=<someformat>,to=<someformat>, format=<describe previous types>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import runinfo_api
from hep.crest.client.model.run_lumi_set_dto import RunLumiSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = runinfo_api.RuninfoApi(api_client)
    since = "none" # str | since: the starting time or run-lumi (optional) if omitted the server will use the default value of "none"
    until = "none" # str | until: the ending time or run-lumi (optional) if omitted the server will use the default value of "none"
    format = "number" # str | the format to digest previous arguments [iso], [number], [run-lumi]. Time(iso) = yyyymmddhhmiss,  Time(number) = milliseconds or Run(number) = runnumber Run(run-lumi) = runnumber-lumisection  (optional) if omitted the server will use the default value of "number"
    mode = "runrange" # str | the mode for the request : [daterange] or [runrange]  (optional) if omitted the server will use the default value of "runrange"
    page = 0 # int | page: the page number {0} (optional) if omitted the server will use the default value of 0
    size = 1000 # int | size: the page size {1000} (optional) if omitted the server will use the default value of 1000
    sort = "id.runNumber:ASC" # str | sort: the sort pattern {id.runNumber:ASC} (optional) if omitted the server will use the default value of "id.runNumber:ASC"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a RunLumiInfoDto lists using parameters.
        api_response = api_instance.list_run_info(since=since, until=until, format=format, mode=mode, page=page, size=size, sort=sort)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling RuninfoApi->list_run_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **str**| since: the starting time or run-lumi | [optional] if omitted the server will use the default value of "none"
 **until** | **str**| until: the ending time or run-lumi | [optional] if omitted the server will use the default value of "none"
 **format** | **str**| the format to digest previous arguments [iso], [number], [run-lumi]. Time(iso) &#x3D; yyyymmddhhmiss,  Time(number) &#x3D; milliseconds or Run(number) &#x3D; runnumber Run(run-lumi) &#x3D; runnumber-lumisection  | [optional] if omitted the server will use the default value of "number"
 **mode** | **str**| the mode for the request : [daterange] or [runrange]  | [optional] if omitted the server will use the default value of "runrange"
 **page** | **int**| page: the page number {0} | [optional] if omitted the server will use the default value of 0
 **size** | **int**| size: the page size {1000} | [optional] if omitted the server will use the default value of 1000
 **sort** | **str**| sort: the sort pattern {id.runNumber:ASC} | [optional] if omitted the server will use the default value of "id.runNumber:ASC"

### Return type

[**RunLumiSetDto**](RunLumiSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_run_info**
> RunLumiSetDto update_run_info()

Update an entry for run information.

Run informations go into a separate table. To update an entry, the run number and the lumi section must be provided.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import runinfo_api
from hep.crest.client.model.run_lumi_info_dto import RunLumiInfoDto
from hep.crest.client.model.run_lumi_set_dto import RunLumiSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = runinfo_api.RuninfoApi(api_client)
    run_lumi_info_dto = RunLumiInfoDto(
        run_number=3.14,
        lb=3.14,
        starttime=3.14,
        endtime=3.14,
    ) # RunLumiInfoDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update an entry for run information.
        api_response = api_instance.update_run_info(run_lumi_info_dto=run_lumi_info_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling RuninfoApi->update_run_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **run_lumi_info_dto** | [**RunLumiInfoDto**](RunLumiInfoDto.md)|  | [optional]

### Return type

[**RunLumiSetDto**](RunLumiSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

