# hep.crest.client.FoldersApi

All URIs are relative to *https://crest-undertow.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_folder**](FoldersApi.md#create_folder) | **POST** /folders | Create an entry for folder information.
[**list_folders**](FoldersApi.md#list_folders) | **GET** /folders | Finds a FolderDto list.


# **create_folder**
> FolderDto create_folder()

Create an entry for folder information.

Folder informations go into a dedicated table.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import folders_api
from hep.crest.client.model.folder_dto import FolderDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = folders_api.FoldersApi(api_client)
    folder_dto = FolderDto(
        node_fullpath="node_fullpath_example",
        schema_name="schema_name_example",
        node_name="node_name_example",
        node_description="node_description_example",
        tag_pattern="tag_pattern_example",
        group_role="group_role_example",
    ) # FolderDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create an entry for folder information.
        api_response = api_instance.create_folder(folder_dto=folder_dto)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling FoldersApi->create_folder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder_dto** | [**FolderDto**](FolderDto.md)|  | [optional]

### Return type

[**FolderDto**](FolderDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_folders**
> FolderSetDto list_folders()

Finds a FolderDto list.

This method allows to perform search and sorting.Arguments: by=<pattern>, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import hep.crest.client
from hep.crest.client.api import folders_api
from hep.crest.client.model.folder_set_dto import FolderSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://crest-undertow.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = hep.crest.client.Configuration(
    host = "https://crest-undertow.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = hep.crest.client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with hep.crest.client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = folders_api.FoldersApi(api_client)
    schema = "none" # str | the schema pattern {none} (optional) if omitted the server will use the default value of "none"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a FolderDto list.
        api_response = api_instance.list_folders(schema=schema)
        pprint(api_response)
    except hep.crest.client.ApiException as e:
        print("Exception when calling FoldersApi->list_folders: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **schema** | **str**| the schema pattern {none} | [optional] if omitted the server will use the default value of "none"

### Return type

[**FolderSetDto**](FolderSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

