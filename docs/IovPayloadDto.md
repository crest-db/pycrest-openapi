# IovPayloadDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**since** | **float** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**version** | **str** |  | [optional] 
**object_type** | **str** |  | [optional] 
**object_name** | **str** |  | [optional] 
**compression_type** | **str** |  | [optional] 
**size** | **int** |  | [optional] 
**payload_hash** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


