"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import hep.crest.client
from hep.crest.client.model.crest_base_response import CrestBaseResponse
from hep.crest.client.model.folder_dto import FolderDto
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.resp_page import RespPage
globals()['CrestBaseResponse'] = CrestBaseResponse
globals()['FolderDto'] = FolderDto
globals()['GenericMap'] = GenericMap
globals()['RespPage'] = RespPage
from hep.crest.client.model.folder_set_dto import FolderSetDto


class TestFolderSetDto(unittest.TestCase):
    """FolderSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testFolderSetDto(self):
        """Test FolderSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = FolderSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
