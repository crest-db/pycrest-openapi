"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import hep.crest.client
from hep.crest.client.model.crest_base_response import CrestBaseResponse
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.resp_page import RespPage
from hep.crest.client.model.store_dto import StoreDto
globals()['CrestBaseResponse'] = CrestBaseResponse
globals()['GenericMap'] = GenericMap
globals()['RespPage'] = RespPage
globals()['StoreDto'] = StoreDto
from hep.crest.client.model.store_set_dto import StoreSetDto


class TestStoreSetDto(unittest.TestCase):
    """StoreSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStoreSetDto(self):
        """Test StoreSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StoreSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
