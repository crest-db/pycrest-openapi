"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import unittest

import hep.crest.client
from hep.crest.client.api.payloads_api import PayloadsApi  # noqa: E501


class TestPayloadsApi(unittest.TestCase):
    """PayloadsApi unit test stubs"""

    def setUp(self):
        self.api = PayloadsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_payload(self):
        """Test case for get_payload

        Finds a payload resource associated to the hash.  # noqa: E501
        """
        pass

    def test_list_payloads(self):
        """Test case for list_payloads

        Finds Payloads metadata.  # noqa: E501
        """
        pass

    def test_store_payload_batch(self):
        """Test case for store_payload_batch

        Create Payloads in the database, associated to a given iov since list and tag name.  # noqa: E501
        """
        pass

    def test_update_payload(self):
        """Test case for update_payload

        Update a streamerInfo in a payload  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
