"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import hep.crest.client
from hep.crest.client.model.folder_set_dto import FolderSetDto
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.global_tag_set_dto import GlobalTagSetDto
from hep.crest.client.model.iov_payload_set_dto import IovPayloadSetDto
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.payload_set_dto import PayloadSetDto
from hep.crest.client.model.payload_tag_info_set_dto import PayloadTagInfoSetDto
from hep.crest.client.model.resp_page import RespPage
from hep.crest.client.model.run_lumi_set_dto import RunLumiSetDto
from hep.crest.client.model.store_set_dto import StoreSetDto
from hep.crest.client.model.tag_meta_set_dto import TagMetaSetDto
from hep.crest.client.model.tag_set_dto import TagSetDto
from hep.crest.client.model.tag_summary_set_dto import TagSummarySetDto
globals()['FolderSetDto'] = FolderSetDto
globals()['GenericMap'] = GenericMap
globals()['GlobalTagMapSetDto'] = GlobalTagMapSetDto
globals()['GlobalTagSetDto'] = GlobalTagSetDto
globals()['IovPayloadSetDto'] = IovPayloadSetDto
globals()['IovSetDto'] = IovSetDto
globals()['PayloadSetDto'] = PayloadSetDto
globals()['PayloadTagInfoSetDto'] = PayloadTagInfoSetDto
globals()['RespPage'] = RespPage
globals()['RunLumiSetDto'] = RunLumiSetDto
globals()['StoreSetDto'] = StoreSetDto
globals()['TagMetaSetDto'] = TagMetaSetDto
globals()['TagSetDto'] = TagSetDto
globals()['TagSummarySetDto'] = TagSummarySetDto
from hep.crest.client.model.crest_base_response import CrestBaseResponse


class TestCrestBaseResponse(unittest.TestCase):
    """CrestBaseResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCrestBaseResponse(self):
        """Test CrestBaseResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = CrestBaseResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
