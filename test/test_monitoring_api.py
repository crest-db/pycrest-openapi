"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import unittest

import hep.crest.client
from hep.crest.client.api.monitoring_api import MonitoringApi  # noqa: E501


class TestMonitoringApi(unittest.TestCase):
    """MonitoringApi unit test stubs"""

    def setUp(self):
        self.api = MonitoringApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_payload_tag_info(self):
        """Test case for list_payload_tag_info

        Retrieves monitoring information on payload as a list of PayloadTagInfoDtos.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
