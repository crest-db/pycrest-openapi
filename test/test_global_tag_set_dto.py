"""
    CREST Server

    OpenApi3 for CREST Server  # noqa: E501

    The version of the OpenAPI document: 4.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import hep.crest.client
from hep.crest.client.model.crest_base_response import CrestBaseResponse
from hep.crest.client.model.generic_map import GenericMap
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from hep.crest.client.model.resp_page import RespPage
globals()['CrestBaseResponse'] = CrestBaseResponse
globals()['GenericMap'] = GenericMap
globals()['GlobalTagDto'] = GlobalTagDto
globals()['RespPage'] = RespPage
from hep.crest.client.model.global_tag_set_dto import GlobalTagSetDto


class TestGlobalTagSetDto(unittest.TestCase):
    """GlobalTagSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGlobalTagSetDto(self):
        """Test GlobalTagSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GlobalTagSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
