#_____________________________________________________________________________
#
# Henri Louvin - December 2018
#
# Recipe to build an alpine image with preisntalled package svom.messaging
# package for Svom NATS/NATS Streaming messaging
#
# Optimized:
#      * Most RUN commands are grouped in a single one to minimize
#        the number of layers and allow for clean-up after compilation
# ___________________________________________________________________________

# start from minimal image
FROM python:3.11

# install dependencies
RUN apt-get update && apt-get install -y netcat-traditional && apt-get install -y jq

# set up workdir
COPY hep /home/hep
COPY setup.py /home/setup.py
COPY README.md /home/README.md
COPY requirements.txt /home/requirements.txt

WORKDIR /home/

RUN pip3 install . \
    # clean up
    && rm -rf ${HOME}/.cache/pip

CMD ["bash"]
